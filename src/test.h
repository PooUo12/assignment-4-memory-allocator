
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

bool test_plain_memory_allocation();

bool test_release_one_block();

bool test_release_several_blocks();

bool test_grow_heap();

bool test_grow_heap_not_near();

void run_test(int i);


