#define _DEFAULT_SOURCE

#include "test.h"

bool test_plain_memory_allocation() {

    void *heap = heap_init(4096);
    if (heap == NULL) return false;
    debug_heap(stdout, heap);

    void *block = _malloc(512);
    if (block == NULL) return false;
    debug_heap(stdout, heap);
    _free(block);
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    return true;
}

bool test_release_one_block() {
    void *heap = heap_init(4096);
    if (heap == NULL) return false;
    void *block1 = _malloc(128);
    void *block2 = _malloc(128);
    if (block1 == NULL || block2 == NULL) return false;
    debug_heap(stdout, heap);
    _free(block1);
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    return true;

}

bool test_release_several_blocks() {
    void *heap = heap_init(4096);
    if (heap == NULL) return false;
    void *block1 = _malloc(128);
    void *block2 = _malloc(128);
    void *block3 = _malloc(128);
    if (block1 == NULL || block2 == NULL || block3 == NULL) return false;
    debug_heap(stdout, heap);
    _free(block1);
    debug_heap(stdout, heap);
    _free(block3);
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    return true;

}

bool test_grow_heap() {
    void *heap = heap_init(1);
    if (heap == NULL) return false;
    debug_heap(stdout, heap);
    void *block1 = _malloc(REGION_MIN_SIZE + 512);
    if (block1 == NULL) return false;
    debug_heap(stdout, heap);
    _free(block1);
    munmap(heap, 2 * REGION_MIN_SIZE);
    return true;
}

bool test_grow_heap_not_near() {
    void *heap = heap_init(1);
    if (heap == NULL) return false;
    debug_heap(stdout, heap);
    (void) mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    void *block1 = _malloc(4096);
    if (block1 == NULL) return false;
    debug_heap(stdout, heap);
    munmap(heap, REGION_MIN_SIZE);
    return true;

}



void run_test(int i) {
    printf("______________________________________________\n");
    printf("Test %d:\n", i);
    bool flag;
    if (i == 1){
        flag = test_plain_memory_allocation();
    }
    else if (i == 2){
        flag = test_release_one_block();
    }
    else if (i == 3){
        flag = test_release_several_blocks();
    }
    else if (i == 4){
        flag = test_grow_heap();
    }
    else if (i == 5){
        flag = test_grow_heap_not_near();
    }
    else{
        fprintf(stderr, "Unknown test\n");
        return;
    }
    if (flag){
        fprintf(stderr, "Test %d passed\n", i);
        }
    else {
        fprintf(stderr, "Test %d failed\n", i);
    }
}
